import {SessionInfo} from './session-info';
import {Injectable} from '@angular/core';
import {License} from '../model/account/license/license';
import {PaymentMethod} from '../model/payment-method/payment-method';
import {Organization} from '../model/organization/organization';
import {OrganizationType} from '../model/organization/organization-type';
import {ProductOrder} from '../model/order/product-order';
import {LicenseProduct} from '../model/product/license-product';
import {Feature} from '../model/product/feature';
import {UserInfo} from '../model/account/user-info';
import {Order} from '../model/product/request/order';
import {RoleInfo} from '../model/account/role/role-info';

@Injectable()
export class DataStorageService {

    private sessionInfo: SessionInfo = new SessionInfo();

    private rolesKey: string  = 'roles';
    private orderLicenseKey: string  = 'orderLicense';
    private licensesKey: string  = 'licenses';
    private authenticatedUserKey: string  = 'authenticatedUser';
    private authoritiesKey: string  = 'authorities';
    private currentLicenseProductKey: string  = 'currentLicenseProduct';
    private featuresKey: string  = 'features';
    private licenseProductsKey: string  = 'licenseProducts';
    private productOrdersKey: string  = 'productOrders';
    private organizationTypesKey: string  = 'organizationTypes';
    private licenseKey: string  = 'license';
    private organizationKey: string  = 'organization';
    private paymentMethodsKey: string  = 'paymentMethods';
    private defaultPaymentMethodKey: string  = 'defaultPaymentMethod';
    private currentPaymentMethodKey: string  = 'currentPaymentMethod';

    set roles(roles: RoleInfo[]) {
        this.sessionInfo.roles = roles;
        sessionStorage.setItem(this.rolesKey, JSON.stringify(roles));
    }

    get roles(): RoleInfo[] {
        let roles = sessionStorage.getItem(this.rolesKey);
        if (roles == null)
            return null;
        return JSON.parse(roles) as RoleInfo[];
    }

    set orderLicense(orderLicense: Order) {
        this.sessionInfo.orderLicense = orderLicense;
        sessionStorage.setItem(this.orderLicenseKey, JSON.stringify(orderLicense));
    }

    get orderLicense(): Order {
        let orderLicense = sessionStorage.getItem(this.orderLicenseKey);
        if (orderLicense == null)
            return null;
        return JSON.parse(orderLicense) as Order;
    }

    set licenses(licenses: License[]) {
        this.sessionInfo.licenses = licenses;
        sessionStorage.setItem(this.licensesKey, JSON.stringify(licenses));
    }

    get licenses(): License[] {
        let licensesPage = sessionStorage.getItem(this.licensesKey);
        if (licensesPage == null)
            return null;
        return JSON.parse(licensesPage) as License[];
    }

    set authorities(authorities: string[]) {
        this.sessionInfo.authorities = authorities;
        sessionStorage.setItem(this.authoritiesKey, JSON.stringify(authorities));
    }

    get authorities(): string[] {
        let authorities = sessionStorage.getItem(this.authoritiesKey);
        if (authorities == null)
            return null;
        return JSON.parse(authorities) as string[];
    }

    set authenticatedUser(authenticatedUser: UserInfo) {
        this.sessionInfo.authenticatedUser = authenticatedUser;
        sessionStorage.setItem(this.authenticatedUserKey, JSON.stringify(authenticatedUser));
    }

    get authenticatedUser(): UserInfo {
        let authenticatedUser = sessionStorage.getItem(this.authenticatedUserKey);
        if (authenticatedUser == null)
            return null;
        return JSON.parse(authenticatedUser) as UserInfo;
    }

    set currentLicenseProduct(currentLicenseProduct: LicenseProduct) {
        this.sessionInfo.currentLicenseProduct = currentLicenseProduct;
        sessionStorage.setItem(this.currentLicenseProductKey, JSON.stringify(currentLicenseProduct));
    }

    get currentLicenseProduct(): LicenseProduct {
        let currentLicenseProduct = sessionStorage.getItem(this.currentLicenseProductKey);
        if (currentLicenseProduct == null)
            return null;
        return JSON.parse(currentLicenseProduct) as LicenseProduct;
    }

    set features(features: Feature[]) {
        this.sessionInfo.features = features;
        sessionStorage.setItem(this.featuresKey, JSON.stringify(features));
    }

    get features(): Feature[] {
        let features = sessionStorage.getItem(this.featuresKey);
        if (features == null)
            return null;
        return JSON.parse(features) as Feature[];
    }

    set licenseProducts(licenseProducts: LicenseProduct[]) {
        this.sessionInfo.licenseProducts = licenseProducts;
        sessionStorage.setItem(this.licenseProductsKey, JSON.stringify(licenseProducts));
    }

    get licenseProducts(): LicenseProduct[] {
        let licenseProducts = sessionStorage.getItem(this.licenseProductsKey);
        if (licenseProducts == null)
            return null;
        return JSON.parse(licenseProducts) as LicenseProduct[];
    }

    set productOrders(productOrders: ProductOrder[]) {
        this.sessionInfo.productOrders = productOrders;
        sessionStorage.setItem(this.productOrdersKey, JSON.stringify(productOrders));
    }

    get productOrders(): ProductOrder[] {
        let productOrders = sessionStorage.getItem(this.productOrdersKey);
        if (productOrders == null)
            return null;
        return JSON.parse(productOrders) as ProductOrder[];
    }

    set organizationTypes(organizationTypes: OrganizationType[]) {
        this.sessionInfo.organizationTypes = organizationTypes;
        sessionStorage.setItem(this.organizationTypesKey, JSON.stringify(organizationTypes));
    }

    get organizationTypes(): OrganizationType[] {
        let organizationTypes = sessionStorage.getItem(this.organizationTypesKey);
        if (organizationTypes == null)
            return null;
        return JSON.parse(organizationTypes) as OrganizationType[];
    }

    set license(license: License) {
        this.sessionInfo.license = license;
        sessionStorage.setItem(this.licenseKey, JSON.stringify(license));
    }

    get license(): License {
        let license = sessionStorage.getItem(this.licenseKey);
        if (license == null)
            return null;
        return JSON.parse(license) as License;
    }

    set organization(organization: Organization) {
        this.sessionInfo.organization = organization;
        sessionStorage.setItem(this.organizationKey, JSON.stringify(organization));
    }

    get organization(): Organization {
        let organization = sessionStorage.getItem(this.organizationKey);
        if (organization == null)
            return null;
        return JSON.parse(organization) as Organization;
    }

    set paymentMethods(paymentMethods: PaymentMethod[]) {
        this.sessionInfo.paymentMethods = paymentMethods;
        sessionStorage.setItem(this.paymentMethodsKey, JSON.stringify(paymentMethods));
    }

    get paymentMethods() {
        let paymentMethods = sessionStorage.getItem(this.paymentMethodsKey);
        if (paymentMethods == null)
            return null;
        return JSON.parse(paymentMethods) as PaymentMethod[];
    }

    set defaultPaymentMethod(paymentMethod: PaymentMethod) {
        this.sessionInfo.defaultPaymentMethod = paymentMethod;
        sessionStorage.setItem(this.defaultPaymentMethodKey, JSON.stringify(paymentMethod));
    }

    get defaultPaymentMethod() {
        let paymentMethod = sessionStorage.getItem(this.defaultPaymentMethodKey);
        if (paymentMethod == null)
            return null;
        return JSON.parse(paymentMethod) as PaymentMethod;
    }

    set paymentMethodUpdate(paymentMethod: PaymentMethod) {
        this.sessionInfo.currentPaymentMethod = paymentMethod;
        sessionStorage.setItem(this.currentPaymentMethodKey, JSON.stringify(paymentMethod));
    }

    get paymentMethodUpdate() {
        let paymentMethod = sessionStorage.getItem(this.currentPaymentMethodKey);
        if (paymentMethod == null)
            return null;
        return JSON.parse(paymentMethod) as PaymentMethod;
    }

    clearAll() {
        sessionStorage.clear();
    }

    isReady(): boolean {

        let validations: Map<string, boolean> = new Map<string, boolean>();

        validations.set(this.rolesKey, sessionStorage[this.rolesKey] != null);
        validations.set(this.licensesKey, sessionStorage[this.licensesKey] != null);
        validations.set(this.licenseKey, sessionStorage[this.licenseKey] != null);
        validations.set(this.defaultPaymentMethodKey, sessionStorage[this.defaultPaymentMethodKey] != null);
        validations.set(this.paymentMethodsKey, sessionStorage[this.paymentMethodsKey] != null);
        validations.set(this.organizationKey, sessionStorage[this.organizationKey] != null);
        validations.set(this.organizationTypesKey, sessionStorage[this.organizationTypesKey]!= null);
        validations.set(this.productOrdersKey, sessionStorage[this.productOrdersKey] != null);
        validations.set(this.licenseProductsKey, sessionStorage[this.licenseProductsKey] != null);
        validations.set(this.featuresKey, sessionStorage[this.featuresKey] != null);

        let size = validations.size;
        let currentValidations = 0;

        validations.forEach((value, key) => {
            if (value)
                currentValidations++;
        });

        console.log("currentValidations: " + currentValidations + " of " + size);

        return currentValidations == size;
    }

}
