
export class OrderPayment {

    alias: string;
    transactionIdentifier: string;
    authorizationCode: string;
    responseCode: string;
    prosaCode: string;
    traceNumber: string;
    message: string;
    amount: string;
    bin: string;
    createdAt: string;

}
