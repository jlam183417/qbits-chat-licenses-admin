import {OrderPayment} from './order-payment';
import {Organization} from '../organization/organization';
import {PaymentMethod} from '../payment-method/payment-method';
import {OrderDetail} from './order-detail';
import {OrderType} from './order-type';

export class ProductOrder {

    id: number;
    createdAt: string;
    total: number;
    status: string;
    type: OrderType;
    orderPayment: OrderPayment;
    organization: Organization;
    paymentMethod: PaymentMethod;

    firstDetail: OrderDetail;
    orderDetails: OrderDetail[];

}
