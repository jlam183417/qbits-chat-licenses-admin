
export class OrganizationType {

    identifier: string;
    description: string;
    visible: boolean;

}
