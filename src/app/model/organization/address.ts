
export class Address {

    address: string;
    state: string;
    city: string;
    zipCode: string;

    description() {
        return `${this.address}, ${this.city}, 
                ${this.state}, ${this.zipCode}`;
    }

}
