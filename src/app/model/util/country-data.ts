
export class CountryData {

    name: string;
    status: string;
    description: string;
    countryCallingCode: string;
    alpha2: string;

}
