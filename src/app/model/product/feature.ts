
export class Feature {
    id: number = null;
    code: string = '';
    description: string = '';
}
