import {Cost} from './cost';

export class LicenseProductInfo {

    id: number;
    code: string;
    name: string;
    description: string;
    shortDescription: string;
    abbreviation: string;
    iconUrl: string;
    color: string;
    type: string;

    calculatedCostStr: string;
    cost: Cost;

}
