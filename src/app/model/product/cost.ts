import { Tax } from './tax';

export class Cost {

    value: number = 0.0;
    tax: Tax = null;

    constructor() {
        this.tax = new Tax();
        this.tax.value = 16.0;
    }

}
