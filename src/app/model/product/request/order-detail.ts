import { Settings } from '../../../config/settings';
import { LicenseProduct } from '../license-product';

export class OrderDetail {

    durationId: number;
    quantity: number;
    productId: number;
    subtotal: number;
    licenseProduct: LicenseProduct;

    constructor(licenseProduct: LicenseProduct, quantity: number) {
        this.licenseProduct = licenseProduct;
        this.productId = licenseProduct.id;
        this.quantity = quantity;
        this.durationId = Settings.defaultLicenseDurationId;
        this.calculateSubtotal();
    }

    incrementQuantity() {
        this.quantity++;
        this.calculateSubtotal();
    }

    decrementQuantity() {
        this.quantity--;
        this.calculateSubtotal();
    }

    calculateSubtotal() {
        this.subtotal = this.quantity * this.licenseProduct.calculatedCost;
    }

}
