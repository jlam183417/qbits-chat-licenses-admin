import {Cost} from './cost';
import {Feature} from './feature';

export class LicenseProduct {

    id: number;
    code: string;
    name: string;
    description: string;
    recordingUrl: string;
    iconUrl: string;
    color: string;
    type: string;
    modified: boolean;

    calculatedCostStr: string;
    calculatedCost: number;
    cost: Cost;
    features: Feature[];

    selected(): boolean {
        return this.id !== undefined;
    }

}
