export class RoleInfo {

    id: number;
    code: string;
    name: string;
    description: string;

}
