export enum Role {
    Owner = 'OWNER',
    Admin = 'ADMIN',
    User = 'USER',
    Salesman = 'SALESMAN',
    Validator = 'VALIDATOR'
}
