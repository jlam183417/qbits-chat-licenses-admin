
export class LicenseCancellationRequest {

    licenseId: number;
    quantity: number = 1;

    incrementQuantity() {
        this.quantity++;
    }

    decrementQuantity() {
        this.quantity--;
    }

}
