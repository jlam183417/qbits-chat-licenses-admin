export class LicenseUpdateRequest {

    licenseId: number;
    productId: number;
    paymentMethodIdentifier: string;

    constructor(licenseId: number, productId: number) {
        this.licenseId = licenseId;
        this.productId = productId;
    }

}
