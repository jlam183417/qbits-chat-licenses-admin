import {Cellphone} from './cellphone';

export class UserRequest {

    name: string;
    lastName: string;
    secondLastName: string;
    birthdaySt: string;
    cellPhone: Cellphone;
    email: string;
    genderReferenceId: number;
    fullName: string;
    password: string;
    confirmPassword: string;

    constructor(email: string, fullName: string, password: string,
                birthdateSt: string, genderReferenceId: number, cellPhone: Cellphone) {

        this.email = email;
        this.fullName = fullName;
        this.password = password;
        this.confirmPassword = password;
        this.birthdaySt = birthdateSt;
        this.genderReferenceId = genderReferenceId;
        this.cellPhone = cellPhone;

        let splitName = fullName.split(" ");

        if (splitName.length > 0)
            this.name = splitName[0];

        if (splitName.length > 1)
            this.lastName = splitName[1];

        if (splitName.length > 2)
            this.secondLastName = splitName[2];

    }

}
