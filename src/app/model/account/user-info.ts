import {RoleInfo} from './role/role-info';
import {Cellphone} from './activate/cellphone';

export class UserInfo {

    id: number;
    jid: string;
    email: string;
    enabled: string;
    name: string;
    lastName: string;
    secondLastName: string;
    fullName: string;
    birthday: string;
    genderReference: GenderReference;
    nickname: string;
    phone: Cellphone;
    phoneRegion: string;
    mainRole: RoleInfo;
    memberSince: string;
    authorities: string[] = [];

}

class GenderReference {
    identifier: string;
    description: string;
}
