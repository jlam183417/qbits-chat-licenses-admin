
export class PaymentMethodRequest {

    firstName: string;
    lastName: string;
    cardNumber: string;
    expirationDate: string;
    securityCode: string;
    customerName: string;
    defaultMethod: boolean = false;

    constructor() {
        // this.firstName = 'Monica';
        // this.lastName = 'Salazar';
        // this.cardNumber = '4242 4242 4242 4242';
        // this.expirationDate = '10 / 22';
        // this.securityCode = '123';
    }

}
