
export class Settings {

    public static emptyValue: string = 'empty';
    public static defaultLocale: string = 'es';

    public static defaultFullAge: number = 18;
    public static defaultQuantity: number = 1;
    public static defaultLicenseDurationId: number = 1;
    public static defaultDateTimeFormat: string = 'DD-MM-YYYY';

}
