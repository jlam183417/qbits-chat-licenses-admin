import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout';
import { AccountComponent } from './components/account/account.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { PaymentMethodComponent } from './components/payment-method/payment-method.component';
import { AddPaymentMethodFormComponent } from './components/payment-method/components/create/add-payment-method-form.component';
import { BillingInformationComponent } from './components/billing-information/billing-information.component';
import { ChangeEmailAccountComponent } from './components/change-email-account/change-email-account.component';
import { ChangeMembershipComponent } from './components/change-membership/change-membership.component';
import { EditPaymentMethodFormComponent } from './components/payment-method/components/edit/edit-payment-method-form.component';
import { SaleInvitationsComponent } from './components/sale-invitations/sale-invitations.component';
import { MembershipResumeComponent } from './components/change-membership/components/membership-resume/membership-resume.component';
import { UserLicensesManagerComponent } from './components/user-licenses-manager/user-licenses-manager.component';
import { LicenseProductVisualizerComponent } from './components/license-product-visualizer/license-product-visualizer.component';
import { ActivateUserComponent } from './components/non-authenticated/components/activate-user/activate-user.component';
import { CancelMembershipComponent } from './components/cancel-membership/cancel-membership.component';
import {RestorePasswordComponent} from './components/non-authenticated/components/restore-password/restore-password.component';
import {ForgotPasswordComponent} from './components/non-authenticated/components/forgot-password/forgot-password.component';

export const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: 'account', component: AccountComponent },
            { path: 'change-password', component: ChangePasswordComponent },
            { path: 'activate-user', component: ActivateUserComponent },
            { path: 'restore-password', component: RestorePasswordComponent },
            { path: 'forgot-password', component: ForgotPasswordComponent },
            { path: 'billing-information', component: BillingInformationComponent },
            { path: 'change-email-account', component: ChangeEmailAccountComponent },
            { path: 'cancel-membership', component: CancelMembershipComponent },
            { path: 'change-membership', component: ChangeMembershipComponent },
            { path: 'membership-resume', component: MembershipResumeComponent },
            { path: 'payment-method', component: PaymentMethodComponent },
            { path: 'add-payment-method', component: AddPaymentMethodFormComponent },
            { path: 'edit-payment-method', component: EditPaymentMethodFormComponent },
            { path: 'invitation', component: SaleInvitationsComponent },
            { path: 'user-licenses-manager', component: UserLicensesManagerComponent },
            { path: 'license-product-visualizer', component: LicenseProductVisualizerComponent },
            { path: '', loadChildren: () => import('./components/auth/auth.module').then(module => module.AuthModule) },
            { path: '**', redirectTo: '/404?message=Invalid route' }
        ]
    },
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);
