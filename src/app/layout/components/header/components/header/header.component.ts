import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../../components/auth/service/auth.service';
import { DataStorageService } from '../../../../../data/data-storage.service';
import {UserInfo} from '../../../../../model/account/user-info';
import {UserService} from '../../../../../service/user.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['header.component.css']
})
export class HeaderComponent implements OnInit {

    authenticated: boolean = false;
    authenticatedUser: UserInfo;

    constructor(private router: Router,
                private authService: AuthService,
                private userService: UserService,
                private spinner: NgxSpinnerService,
                private dataStorageService: DataStorageService) { }

    ngOnInit() {
        this.authenticated = this.authService.isAuthenticated();
        this.authenticatedUser = this.dataStorageService.authenticatedUser;
        if (this.authenticated && this.authenticatedUser == null) {
            this.userService.userInfo().subscribe(user => {
                this.dataStorageService.authenticatedUser = user;
                this.authenticatedUser = user;
            });
        }
    }

    logOut() {

        this.spinner.show('logoutSpinner');

        setTimeout(() => {

            this.authService.logOut();
            sessionStorage.clear();

            this.router.navigate(['/login']);
            this.spinner.hide('logoutSpinner');

        }, 1500);


    }

}
