import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

import {ProductOrder} from '../model/order/product-order';
import {SimpleMessage} from '../model/util/simple-message';
import {LicenseOrderRequest} from '../model/product/request/license-order-request';

@Injectable({
    providedIn: 'root'
})
export class ProductOrderService {

    constructor(protected http: HttpClient) {

    }

    findOrders(organizationId: number): Observable<ProductOrder[]> {

        const url = `${environment.chatLicensesUrl}/order/organization/${organizationId}`;

        return this.http.get<ProductOrder[]>(url);
    }

    createLicenseOrder(licenseOrder: LicenseOrderRequest): Observable<SimpleMessage> {

        const url = `${environment.chatLicensesUrl}/order/organization/${licenseOrder.organizationId}/license`;

        return this.http.post<SimpleMessage>(url, licenseOrder);
    }

}
