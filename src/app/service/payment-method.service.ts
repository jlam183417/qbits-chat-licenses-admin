import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { PaymentMethod } from '../model/payment-method/payment-method';
import { PaymentMethodRequest } from '../model/payment-method/payment-method-request';
import { PaymentMethodUpdateRequest} from '../model/payment-method/payment-method-update-request';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PaymentMethodService {

    constructor(protected http: HttpClient) {

    }

    createPaymentMethod(organizationId: number, paymentMethod: PaymentMethodRequest) {

        paymentMethod.cardNumber = paymentMethod.cardNumber.replace(/\s/g, "");
        paymentMethod.expirationDate = paymentMethod.expirationDate.replace(/\s/g, "");
        paymentMethod.customerName = `${paymentMethod.firstName} ${paymentMethod.lastName}`;

        const url = `${environment.chatLicensesUrl}/paymentMethod/${organizationId}`;
        return this.http.post(url, paymentMethod);
    }

    findPaymentMethods(organizationId: number): Observable<PaymentMethod[]> {
        const url = `${environment.chatLicensesUrl}/paymentMethod/${organizationId}`;
        return this.http.get<PaymentMethod[]>(url);
    }

    findDefaultPaymentMethod(organizationId: number): Observable<PaymentMethod> {
        const url = `${environment.chatLicensesUrl}/paymentMethod/default/${organizationId}`;
        return this.http.get<PaymentMethod>(url);
    }

    deletePaymentMethod(identifier: string) {
        const url = `${environment.chatLicensesUrl}/paymentMethod/${identifier}`;
        return this.http.delete<PaymentMethod>(url);
    }

    defaultPaymentMethod(identifier: string) {
        const url = `${environment.chatLicensesUrl}/paymentMethod/default/${identifier}`;
        return this.http.post<PaymentMethod>(url, {});
    }

    updatePaymentMethod(identifier: string, paymentMethod: PaymentMethodUpdateRequest) {
        const url = `${environment.chatLicensesUrl}/paymentMethod/${identifier}`;
        return this.http.put<PaymentMethod>(url, paymentMethod);
    }

}
