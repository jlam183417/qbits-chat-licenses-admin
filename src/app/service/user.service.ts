import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserInfo } from '../model/account/user-info';

import { environment } from '../../environments/environment';
import {Page} from '../model/util/page';
import {UserLicenseInfo} from '../model/account/license/user-license-info';
import {EmailConfirmationToken} from '../model/account/activate/confirmation/email-confirmation-token';
import {UserRequest} from '../model/account/activate/user-request';
import {SimpleMessage} from '../model/util/simple-message';
import {ChangePasswordRequest} from '../model/password/change-password-request';
import {RestorePasswordRequest} from '../model/account/password/restore-password-request';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(protected http: HttpClient) {}

    userInfo(): Observable<UserInfo> {
        const url = `${environment.chatLicensesUrl}/user/info`;
        return this.http.get<UserInfo>(url);
    }

    findUsers(search: string, organizationId: number, page: number, size: number): Observable<Page<UserLicenseInfo>> {

        const url = `${environment.chatLicensesUrl}/user/license/${organizationId}`;

        let params = new HttpParams()
            .append('search', search)
            .append('page', page.toString())
            .append('size', size.toString())
            .append('sort', 'id,desc');

        return this.http.get<Page<UserLicenseInfo>>(url, {params: params});
    }

    emailByConfirmationToken(confirmationToken: string): Observable<EmailConfirmationToken> {
        const url = `${environment.chatLicensesUrl}/user/email/confirmation/${confirmationToken}`;
        return this.http.get<EmailConfirmationToken>(url);
    }

    validateRestorePasswordToken(passwordConfirmationToken: string): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/user/password/token/${passwordConfirmationToken}`;
        return this.http.get<SimpleMessage>(url);
    }

    confirmUser(confirmationToken: string, userRequest: UserRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/user/confirm/${confirmationToken}`;
        return this.http.patch<SimpleMessage>(url, userRequest);
    }

    changePassword(changePasswordRequest: ChangePasswordRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/user/changePassword`;
        return this.http.patch<SimpleMessage>(url, changePasswordRequest);
    }

    forgotPassword(email: string): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/user/forgotPassword`;
        return this.http.post<SimpleMessage>(url, {email: email});
    }

    restorePassword(forgotPasswordToken: string,
                    restorePasswordRequest: RestorePasswordRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/user/restorePassword/${forgotPasswordToken}`;
        return this.http.patch<SimpleMessage>(url, restorePasswordRequest);
    }

}
