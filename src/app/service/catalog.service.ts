import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {OrganizationType} from '../model/organization/organization-type';

import {environment} from '../../environments/environment';
import {RoleInfo} from '../model/account/role/role-info';
import {GenderReference} from '../model/account/activate/gender-reference';

@Injectable({
    providedIn: 'root'
})
export class CatalogService {

    constructor(protected http: HttpClient) {

    }

    findOrganizationTypes(): Observable<OrganizationType[]> {
        const url = `${environment.chatLicensesUrl}/catalog/organizationType`;
        return this.http.get<OrganizationType[]>(url);
    }

    findRoles(): Observable<RoleInfo[]> {
        const url = `${environment.chatLicensesUrl}/catalog/role`;
        return this.http.get<RoleInfo[]>(url);
    }

    findGenderReferences(): Observable<GenderReference[]> {
        const endpoint = `${environment.chatLicensesUrl}/catalog/genderReference`;
        return this.http.get<GenderReference[]>(endpoint);
    }

}
