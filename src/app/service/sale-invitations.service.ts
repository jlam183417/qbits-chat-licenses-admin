import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { SaleInvitationInfo } from '../model/invitation/sale-invitation-info';
import { SaleInvitationRequest } from '../model/invitation/sale-invitation-request';
import { SimpleMessage } from '../model/util/simple-message';
import { Page } from '../model/util/page';

@Injectable({
    providedIn: 'root'
})
export class SaleInvitationsService {

    constructor(protected http: HttpClient) {

    }

    findInvitations(search: string, page: number, size: number): Observable<Page<SaleInvitationInfo>> {

        const url = `${environment.chatLicensesUrl}/sale/invitation`;

        let params = new HttpParams()
            .append('search', search)
            .append('page', page.toString())
            .append('size', size.toString())
            .append('sort', 'id,desc');

        return this.http.get<Page<SaleInvitationInfo>>(url, {params: params});
    }

    createInvitation(saleInvitation: SaleInvitationRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/sale/invitation/send`;
        return this.http.post<SimpleMessage>(url, saleInvitation);
    }

    forwardInvitation(saleInvitationId: number): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/sale/invitation/forward/${saleInvitationId}`;
        return this.http.patch<SimpleMessage>(url, {});
    }

    cancelInvitation(saleInvitationId: number) {
        const url = `${environment.chatLicensesUrl}/sale/invitation/cancel/${saleInvitationId}`;
        return this.http.delete<SimpleMessage>(url);
    }

}
