import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {License} from '../model/account/license/license';
import {SimpleMessage} from '../model/util/simple-message';
import {LicenseUpdateRequest} from '../model/account/license/actions/license-update-request';
import {Page} from '../model/util/page';
import {LicenseAssignmentRequest} from '../model/account/license/actions/license-assignment-request';
import {LicenseUnassignRequest} from '../model/account/license/actions/license-unassign-request';
import {LicenseCancellationRequest} from '../model/account/license/actions/license-cancellation-request';

@Injectable({
    providedIn: 'root'
})
export class LicenseService {

    constructor(protected http: HttpClient) {}

    findGroupedLicenses(organizationId: number): Observable<License[]> {
        const url = `${environment.chatLicensesUrl}/license/organization/${organizationId}/group`;
        return this.http.get<License[]>(url);
    }

    findLicenses(organizationId: number, page: number, size: number): Observable<Page<License>> {
        const url = `${environment.chatLicensesUrl}/license/organization/${organizationId}`;

        let params = new HttpParams()
            .append('page', page.toString())
            .append('size', size.toString())
            .append('sort', 'id,desc');

        return this.http.get<Page<License>>(url, {params: params});
    }

    findCurrentLicense(): Observable<License> {
        const url = `${environment.chatLicensesUrl}/license/me`;
        return this.http.get<License>(url);
    }

    updateLicense(licenseUpdate: LicenseUpdateRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/license/update`;
        return this.http.post<SimpleMessage>(url, licenseUpdate);
    }

    assignLicense(licenseAssignment: LicenseAssignmentRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/license/assign`;
        return this.http.post<SimpleMessage>(url, licenseAssignment);
    }

    cancelLicenses(licenseCancellationRequest: LicenseCancellationRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/license/cancel`;
        return this.http.post<SimpleMessage>(url, licenseCancellationRequest);
    }

    unassignLicense(licenseUnassign: LicenseUnassignRequest): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/license/unassign`;
        return this.http.post<SimpleMessage>(url, licenseUnassign);
    }

}
