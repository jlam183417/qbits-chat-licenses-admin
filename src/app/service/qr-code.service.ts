import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { SimpleMessage } from '../model/util/simple-message';
import {SendQrCodeRequest} from '../model/qr/send-qr-code-request';

@Injectable({
    providedIn: 'root'
})
export class QrCodeService {

    constructor(protected http: HttpClient) {

    }

    sendQRCode(email: string): Observable<SimpleMessage> {
        const url = `${environment.chatLicensesUrl}/code/qr/send`;
        return this.http.post<SimpleMessage>(url, new SendQrCodeRequest(email));
    }

}
