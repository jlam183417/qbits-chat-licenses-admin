import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Feature } from '../../../../model/product/feature';
import { LicenseProduct } from '../../../../model/product/license-product';
import {DataStorageService} from '../../../../data/data-storage.service';

@Component ({
    selector: 'license-product-item',
    templateUrl: './license-product-item.component.html',
    styleUrls: ['./license-product-item.component.css']
})
export class LicenseProductItemComponent implements OnInit {

    @Input() public licenseProduct: LicenseProduct;
    @Input() public hasFeatures: Feature[];
    @Input() public features: Feature[];
    @Input() public disabled: boolean;
    @Input() public redirectionUrl: string;

    constructor(private router: Router,
                public dataStorageService: DataStorageService) {
    }

    ngOnInit() {

    }

    submit() {
        this.dataStorageService.currentLicenseProduct = this.licenseProduct;
        this.router.navigate([this.redirectionUrl]);
    }

    hasFeature(feature: Feature) {
        return this.hasFeatures.filter(cFeature =>
            cFeature.code == feature.code).length > 0;
    }

}
