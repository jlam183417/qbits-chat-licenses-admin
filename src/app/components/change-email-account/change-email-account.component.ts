import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-change-email-account',
    templateUrl: './change-email-account.component.html',
    styleUrls: ['./change-email-account.component.css']
})
export class ChangeEmailAccountComponent implements OnInit {

    constructor(private router: Router) {

    }

    ngOnInit() {

    }

    submit() {

    }

    cancel() {
        this.router.navigate(['/account']);
    }

}
