import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';

import { CatalogService } from '../../service/catalog.service';
import { LicenseService } from '../../service/license.service';
import { PaymentMethodService } from '../../service/payment-method.service';
import { OrganizationService } from '../../service/organization.service';
import { DataStorageService } from '../../data/data-storage.service';

import { License } from '../../model/account/license/license';
import { PaymentMethod } from '../../model/payment-method/payment-method';
import { ProductOrderService } from '../../service/product-order.service';
import { NgxSpinnerService } from "ngx-spinner";
import { LicensesProductService } from '../../service/license-product.service';
import { FeaturesService } from '../../service/features.service';
import {UserService} from '../../service/user.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

    license: License = new License();
    defaultPaymentMethod = new PaymentMethod();

    constructor(private router: Router,
                private spinner: NgxSpinnerService,
                public authService: AuthService,
                public featuresService: FeaturesService,
                public userService: UserService,
                public licenseService: LicenseService,
                public licenseProductService: LicensesProductService,
                public catalogService: CatalogService,
                public productOrderService: ProductOrderService,
                public organizationService: OrganizationService,
                public paymentMethodService: PaymentMethodService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);
    }

    ngOnInit() {

        this.license = this.dataStorageService.license;
        this.defaultPaymentMethod = this.dataStorageService.defaultPaymentMethod;

        if (this.license == null) {

            this.spinner.show('accountSpinner');

            this.licenseService.findCurrentLicense().subscribe(license =>  {

                this.license = license;
                this.dataStorageService.license = license;
                this.ready();

                let organizationId = this.license.organizationId;

                this.catalogService.findRoles()
                    .subscribe(roles => {
                        this.dataStorageService.roles = roles;
                        this.ready();
                    });

                this.licenseService.findGroupedLicenses(organizationId)
                    .subscribe(licenses => {
                        this.dataStorageService.licenses = licenses;
                        this.ready();
                    });

                this.paymentMethodService.findDefaultPaymentMethod(organizationId)
                    .subscribe(paymentMethod => {
                        this.defaultPaymentMethod = paymentMethod;
                        this.dataStorageService.defaultPaymentMethod = paymentMethod;
                        this.ready();
                    });

                this.paymentMethodService.findPaymentMethods(organizationId)
                    .subscribe(paymentMethods => {
                        this.dataStorageService.paymentMethods = paymentMethods;
                        this.ready();
                });

                this.organizationService.findOrganizationInfo(organizationId)
                    .subscribe(organization => {
                        this.dataStorageService.organization = organization;
                        this.ready();
                    });

                this.catalogService.findOrganizationTypes()
                    .subscribe(organizationTypes => {
                        this.dataStorageService.organizationTypes = organizationTypes;
                        this.ready();
                });

                this.productOrderService.findOrders(this.license.organizationId)
                    .subscribe(productOrders => {
                        productOrders.forEach(po => {
                            po.firstDetail = po.orderDetails[0];
                        });
                        this.dataStorageService.productOrders = productOrders;
                        this.ready();
                });

                this.licenseProductService.findLicenseProducts()
                    .subscribe(licenseProducts => {
                        this.dataStorageService.licenseProducts = licenseProducts;
                        this.ready();
                    });
                });

                this.featuresService.getFeatures()
                    .subscribe(features => {
                        this.dataStorageService.features = features;
                        this.ready();
                    });
        }

    }

    ready() {
        if (this.dataStorageService.isReady())
            this.spinner.hide('accountSpinner');
    }

    paymentMethod() {

        let navigateUrl: string = '/payment-method';

        this.router.navigate([navigateUrl], {
            state: {
                organizationId: this.license.organizationId
            }
        });

    }

    billingInformation() {
        let navigateUrl: string = '/billing-information';
        this.router.navigate([navigateUrl]);
    }

}
