import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { DataStorageService } from '../../../../data/data-storage.service';
import { LicenseProduct } from '../../../../model/product/license-product';
import { License } from '../../../../model/account/license/license';
import { Utils } from '../../../../util/utils';
import { LicenseUpdateRequest } from '../../../../model/account/license/actions/license-update-request';
import { LicenseService } from '../../../../service/license.service';
import { AuthService } from '../../../auth/service/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component ({
    selector: 'membership-resume',
    templateUrl: './membership-resume.component.html',
    styleUrls: ['./membership-resume.component.css']
})
export class MembershipResumeComponent implements OnInit {

    licenseProduct: LicenseProduct;
    license: License;

    @ViewChild("confirmModal", {static: true}) confirmModal: TemplateRef<any>;

    constructor(private router: Router,
                private spinner: NgxSpinnerService,
                public authService: AuthService,
                private licenseService: LicenseService,
                private modalService: NgbModal,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);
    }

    ngOnInit() {

        this.licenseProduct = this.dataStorageService.currentLicenseProduct;
        this.license = this.dataStorageService.license;

        this.license.describeExpiration = Utils.expirationDescription(this.license);
        this.license.licenseProductInfo.calculatedCostStr = Utils.calculateLicenseCostStr(this.license.licenseProductInfo.cost);

        this.licenseProduct.calculatedCostStr = Utils.calculateLicenseCostStr(this.licenseProduct.cost);
    }

    confirm() {

        let licenseUpdate = new LicenseUpdateRequest(
            this.license.id, this.licenseProduct.id
        );

        this.spinner.show('membershipConfirmationSpinner');

        this.licenseService.updateLicense(licenseUpdate)
            .subscribe((message) => {
                this.licenseService.findCurrentLicense().subscribe(license => {
                    this.dataStorageService.license = license;
                    this.modalService.dismissAll();
                    this.dataStorageService.currentLicenseProduct = null;
                    this.spinner.hide('membershipConfirmationSpinner');
                    this.router.navigate(['/billing-information']);
                });
            })
    }

    cancel() {
        this.modalService.dismissAll();
    }

    goBack() {
        this.router.navigate(['/change-membership']);
    }

    showConfirmation() {

        this.modalService.open(this.confirmModal, {
            ariaLabelledBy: 'modal-basic-title',
            centered: true
        }).result.then((result) => {

        }, (reason) => {

        });

    }

}
