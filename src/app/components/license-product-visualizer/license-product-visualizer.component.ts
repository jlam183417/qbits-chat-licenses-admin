import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';
import { DataStorageService } from '../../data/data-storage.service';

import { Feature } from '../../model/product/feature';
import { LicenseProduct } from '../../model/product/license-product';
import {License} from '../../model/account/license/license';

@Component({
    selector: 'app-license-product-visualizer',
    templateUrl: './license-product-visualizer.component.html',
    styleUrls: ['./license-product-visualizer.component.css']
})
export class LicenseProductVisualizerComponent implements OnInit {

    public licenseProducts: LicenseProduct[];
    public features: Feature[];
    public license: License;

    constructor(private router: Router,
                public authService: AuthService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);
    }

    ngOnInit() {

        this.licenseProducts = this.dataStorageService.licenseProducts;
        this.features = this.dataStorageService.features;

        this.license = this.dataStorageService.license;
    }

}
