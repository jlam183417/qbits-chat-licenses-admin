import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';

import { UserService } from '../../../../service/user.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PinkToast } from '../../../toastr/pink.toast';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

    spinnerText: string;
    loading: boolean = false;
    userEmail: string;

    constructor(private router: Router,
                private userService: UserService,
                private spinner: NgxSpinnerService,
                private modalService: NgbModal,
                private toastr: ToastrService) {
    }

    ngOnInit() {

    }

    submit() {

        this.spinnerText = 'Enviando solicitud de restablecimiento de contraseña...';
        this.spinner.show('forgotPasswordSpinner');

        this.userService.forgotPassword(this.userEmail).subscribe(message => {

            setTimeout(() => {
                this.spinner.hide('forgotPasswordSpinner');
                this.toast('Revise su correo electrónico', 5000);
                this.router.navigate(['/login']);
            }, 400);

        });

    }

    toast(message: string, timeout: number) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = timeout;

        this.toastr.show(message, 'Correcto!', opt);
    }

}
