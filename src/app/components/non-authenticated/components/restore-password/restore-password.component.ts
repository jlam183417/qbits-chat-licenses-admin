import { Component, ElementRef, OnInit, Renderer2, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';

import { UserService } from '../../../../service/user.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PinkToast } from '../../../toastr/pink.toast';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RestorePasswordRequest } from '../../../../model/account/password/restore-password-request';

@Component({
    selector: 'app-restore-password',
    templateUrl: './restore-password.component.html',
    styleUrls: ['./restore-password.component.css']
})
export class RestorePasswordComponent implements OnInit {

    @ViewChild('password', {static: true, read: ElementRef}) password: ElementRef;
    @ViewChild('confirmPassword', {static: true, read: ElementRef}) confirmPassword: ElementRef;

    forgotPasswordToken: string;
    alreadyRestored: boolean = false;
    spinnerText: string;
    loading: boolean = false;

    restorePasswordRequest: RestorePasswordRequest = new RestorePasswordRequest();

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private userService: UserService,
                private spinner: NgxSpinnerService,
                private modalService: NgbModal,
                private toastr: ToastrService,
                private renderer: Renderer2) {
    }

    ngOnInit() {
        this.validateToken();
    }

    validateToken() {

        this.activatedRoute.queryParams.subscribe(params => {

            this.forgotPasswordToken = params.forgotPasswordToken;

            this.spinnerText = 'Cargando...';
            this.spinner.show('restorePasswordSpinner');

            this.loading = true;

            this.userService.validateRestorePasswordToken(this.forgotPasswordToken)
                .subscribe(() => {

                    setTimeout(() => {
                        this.alreadyRestored = false;
                        this.spinner.hide('restorePasswordSpinner');
                        this.loading = false;
                    }, 800);

                }, (err) => {

                    setTimeout(() => {
                        this.loading = false;
                        this.alreadyRestored = true;
                        this.spinner.hide('restorePasswordSpinner');
                    }, 800);

                });
        });

    }

    showPassword(element, icon) {

        let nativeElement = undefined;

        switch (element.name) {
            case 'password':
                nativeElement = this.password.nativeElement; break;
            case 'confirmPassword':
                nativeElement = this.confirmPassword.nativeElement; break;
            default:
                nativeElement = this.confirmPassword.nativeElement; break;
        }

        let type = nativeElement.getAttribute('type');

        if (type === 'text') {

            this.renderer.setAttribute(nativeElement, 'type', 'password');

            this.renderer.addClass(icon, 'fa-eye-slash');
            this.renderer.removeClass(icon, 'fa-eye');

        } else {

            this.renderer.setAttribute(nativeElement, 'type', 'text');

            this.renderer.removeClass(icon, 'fa-eye-slash');
            this.renderer.addClass(icon, 'fa-eye');
        }

    }

    submit() {

        this.spinnerText = 'Restaurando contraseña...';
        this.spinner.show('restorePasswordSpinner');

        this.userService.restorePassword(
            this.forgotPasswordToken, this.restorePasswordRequest
        ).subscribe(message => {

            setTimeout(() => {
                this.alreadyRestored = true;
                this.spinner.hide('restorePasswordSpinner');
                this.toast('Contraseña restaurada correctamente');
            }, 500);

        });

    }

    toast(message: string) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = 1800;

        this.toastr.show(message, 'Correcto!', opt);
    }

}
