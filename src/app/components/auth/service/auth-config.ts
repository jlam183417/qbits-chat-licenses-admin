import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../../../../environments/environment';

export const authConfig: AuthConfig = {
    useHttpBasicAuthForPasswordFlow: true,
    tokenEndpoint: environment.authServerUrl,
    scope: "read write",
    requireHttps: false,
    clientId: 'qbits_chat',
    dummyClientSecret: "chat_s3cr3t"
};
