import { Injectable } from '@angular/core';

import { OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from './auth-config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {EmailConfirmationToken} from '../../../model/account/activate/confirmation/email-confirmation-token';

@Injectable()
export class AuthService {

    constructor(private oauthService: OAuthService,
                protected http: HttpClient) {
        this.configureOAuthService();
    }

    configureOAuthService() {
        if (!this.oauthService.clientId) {
            this.oauthService.configure(authConfig);
        }
    }

    isAuthenticated(): boolean {
        return this.oauthService.hasValidAccessToken();
    }

    login(username: string, password: string) : Promise<any> {
        return this.oauthService.fetchTokenUsingPasswordFlow(username, password);
    }

    checkToken(): Observable<any> {

        const url = `${environment.authServerUrl.replace('token', 'check_token')}`;

        let params = new HttpParams()
            .append('token', this.accessToken);

        return this.http.get<any>(url, {params: params});
    }

    logOut() {
        return this.oauthService.logOut();
    }

    public get accessToken() {
        return this.oauthService.getAccessToken();
    }

}
