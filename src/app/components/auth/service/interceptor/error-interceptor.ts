import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { OAuthService } from 'angular-oauth2-oidc';
import { DataStorageService } from '../../../../data/data-storage.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private oauthService: OAuthService,
        private dataStorageService: DataStorageService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {

            console.log(err);

            if (err.status === 401) {
                this.oauthService.logOut();
                this.dataStorageService.clearAll();
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}
