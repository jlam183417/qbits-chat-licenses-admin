import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { UserService } from '../../../../service/user.service';
import { DataStorageService } from '../../../../data/data-storage.service';

@Component({templateUrl: './auth-login.component.html'})
export class AuthLoginComponent implements OnInit {

    loginForm: FormGroup;

    submitted : boolean = false;
    loading : boolean = false;
    error : string = '';

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private dataStorageService: DataStorageService,
                private authService: AuthService,
                private userService: UserService) {

        authService.configureOAuthService();

        if (authService.isAuthenticated())
            this.router.navigate(['/account']);
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get form() {
        return this.loginForm.controls;
    }

    submit() {

        this.submitted = true;
        if (this.loginForm.invalid) return;

        this.loading = true;

        this.authService.login(
            this.form.username.value, this.form.password.value
        ).then((response) => {

            this.authService.checkToken().subscribe(data => {
                this.dataStorageService.authorities = data.authorities;
            });

            this.userService.userInfo().subscribe(user => {
                this.dataStorageService.authenticatedUser = user;
            });

            this.router.navigate(['/account']);
        }).catch(err => {
            console.error('Error logging in', err);
            this.error = err;
            this.loading = false;
        });

    }

}
