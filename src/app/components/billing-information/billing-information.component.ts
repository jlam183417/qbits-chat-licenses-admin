import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash-es';

import { AuthService } from '../auth/service/auth.service';
import { ProductOrderService } from '../../service/product-order.service';
import { OrganizationService } from '../../service/organization.service';
import { DataStorageService } from '../../data/data-storage.service';
import { ToastrService } from 'ngx-toastr';
import { PinkToast } from '../toastr/pink.toast';

import { OrganizationType } from '../../model/organization/organization-type';
import { ProductOrder } from '../../model/order/product-order';
import { License } from '../../model/account/license/license';
import { Organization } from '../../model/organization/organization';

import { Utils } from '../../util/utils';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-billing-information',
    templateUrl: './billing-information.component.html',
    styleUrls: ['./billing-information.component.css']
})
export class BillingInformationComponent implements OnInit, AfterViewInit {

    @ViewChild("organizationName", {static: false}) organizationName: ElementRef;
    @ViewChild("organizationRfc", {static: false}) organizationRfc: ElementRef;

    modificationButtonName: string = 'Editar';

    organizationTypes: OrganizationType[];
    productOrders: ProductOrder[];
    license: License;
    organization: Organization;
    updatingOrganizationInfo: boolean = false;

    constructor(private router: Router,
                private spinner: NgxSpinnerService,
                private elementRef: ElementRef,
                private toastr: ToastrService,
                private productOrderService: ProductOrderService,
                private organizationService: OrganizationService,
                private authService: AuthService,
                private dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);

    }

    ngAfterViewInit(): void {
        this.elementRef.nativeElement.
            ownerDocument.body.style.backgroundColor = '#fcfcfc';
    }

    ngOnInit() {


        this.license = this.dataStorageService.license;
        this.organization = this.dataStorageService.organization;
        this.organizationTypes = this.dataStorageService.organizationTypes;
        this.productOrders = this.dataStorageService.productOrders;

        this.license.describeExpiration = Utils.expirationDescription(this.license);
        this.license.licenseProductInfo.calculatedCostStr = Utils.calculateLicenseCostStr(this.license.licenseProductInfo.cost);

        this.productOrderService.findOrders(this.license.organizationId)
            .subscribe(productOrders => {
                if (productOrders.length > this.productOrders.length) {
                    this.productOrders = productOrders;
                    productOrders.forEach(po => {
                        po.firstDetail = po.orderDetails[0];
                    });
                    this.dataStorageService.productOrders = productOrders;
                }
            });

    }

    oganizationModification() {

        if (this.modificationButtonName.toLowerCase() == 'guardar') {
            this.updateOrganizationInfo();
            return;
        }

        this.modificationButtonName = 'Guardar';
        this.updatingOrganizationInfo = true;

        this.htmlStyle(this.organizationName, 'color', '#afafaf');
        this.htmlStyle(this.organizationRfc, 'color', '#afafaf');
    }

    updateOrganizationInfo() {

        this.modificationButtonName = 'Editar';

        let name = this.htmlValue(this.organizationName);
        let rfc = this.htmlValue(this.organizationRfc);

        this.organization.name = name.trim();
        this.organization.rfc = rfc.trim();

        this.setHtmlValue(this.organization.name, this.organizationRfc);
        this.setHtmlValue(this.organization.rfc, this.organizationRfc);

        this.organizationService.modifyOrganizationInfo(this.organization)
            .subscribe((res) => {

                this.dataStorageService.organization = this.organization;

                this.htmlStyle(this.organizationName, 'color', '#333');
                this.htmlStyle(this.organizationRfc, 'color', '#333');

                this.updatingOrganizationInfo = false;

                this.toast()
            });

    }

    toast() {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = 1800;

        this.toastr.show('Datos guardados', 'Correcto!', opt);
    }

    htmlStyle(element: ElementRef, prop: string, value: string) {
        element.nativeElement.style[prop] = value;
    }

    htmlValue(element: ElementRef) {
        return element.nativeElement.textContent;
    }

    setHtmlValue(content: string, element: ElementRef) {
        return element.nativeElement.textContent = content;
    }

}
