import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';
import { PaymentMethodService } from '../../service/payment-method.service';
import { PaymentMethod } from '../../model/payment-method/payment-method';
import { DataStorageService } from '../../data/data-storage.service';

import swal from 'sweetalert2';

@Component({
    selector: 'app-payment-method',
    templateUrl: './payment-method.component.html',
    styleUrls: ['./payment-method.component.css']
})
export class PaymentMethodComponent implements OnInit {

    organizationId: number;
    paymentMethods: PaymentMethod[];
    defaultPaymentMethod: PaymentMethod;

    isMobile = false;

    constructor(private router: Router,
                public authService: AuthService,
                public paymentMethodService: PaymentMethodService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);

        this.organizationId = this.dataStorageService.license.organizationId;
    }

    ngOnInit(): void {

        this.paymentMethods = this.dataStorageService.paymentMethods;
        this.defaultPaymentMethod = this.dataStorageService.defaultPaymentMethod;

        this.isMobile = this.getIsMobile();
        window.onresize = () => {
            this.isMobile = this.getIsMobile();
        };

    }

    addPaymentMethod() {

        let navigateUrl: string = '/add-payment-method';

        this.router.navigate([navigateUrl], {
            state: {
                organizationId: this.organizationId
            }
        });

    }

    editPaymentMethod(paymentMethod: PaymentMethod) {

        let navigateUrl: string = '/edit-payment-method';

        this.dataStorageService.paymentMethodUpdate = paymentMethod;

        this.router.navigate([navigateUrl]);
    }

    deletePaymentMethod(paymentMethod: PaymentMethod) {

        if (this.paymentMethods.length <= 1)
            return;

        swal.fire({
            title: '¿Está seguro?',
            text: "Esta operación no se podrá revertir.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#343a40',
            cancelButtonColor: '#8c8c8c',
            confirmButtonText: 'Sí, eliminar'
        }).then((result) => {
            if (result.value) {
                this.paymentMethodService.deletePaymentMethod(paymentMethod.uuid)
                    .subscribe(() => {
                        let index = this.paymentMethods.indexOf(paymentMethod);
                        if (index > -1) {
                            this.paymentMethods.splice(index, 1);
                            let defaultPaymentMethod = this.paymentMethods[0];
                            defaultPaymentMethod.defaultPayment = true;
                            this.dataStorageService.defaultPaymentMethod = defaultPaymentMethod;
                            this.dataStorageService.paymentMethods = this.paymentMethods;
                            swal.fire(
                                '¡Eliminado!',
                                'El método de pago ha sido removido.',
                                'success'
                            )
                        }
                    })
            }
        });
    }

    setDefaultPaymentMethod(paymentMethod: PaymentMethod) {
        swal.fire({
            title: '¿Está seguro?',
            text: "Su método de pago preferido cambiará.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#343a40',
            cancelButtonColor: '#8c8c8c',
            confirmButtonText: 'Sí, cambiarlo',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                this.paymentMethodService.defaultPaymentMethod(paymentMethod.uuid)
                    .subscribe(() => {
                        this.dataStorageService.defaultPaymentMethod = paymentMethod;
                        this.paymentMethods.forEach(pm => {
                            pm.defaultPayment = pm.uuid == paymentMethod.uuid
                        });
                        this.dataStorageService.paymentMethods = this.paymentMethods;
                        swal.fire(
                            '¡Correcto!',
                            'El método de pago preferido ha sido reemplazado.',
                            'success'
                        )
                    });
            } else {
                paymentMethod.defaultPayment = false;
            }
        })
    }

    getIsMobile(): boolean {
        const w = document.documentElement.clientWidth;
        const breakpoint = 576;
        return w < breakpoint;
    }

}
