import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/service/auth.service';
import { SaleInvitationsService } from '../../service/sale-invitations.service';
import { DataStorageService } from '../../data/data-storage.service';
import { SaleInvitationInfo } from '../../model/invitation/sale-invitation-info';
import { SaleInvitationRequest } from '../../model/invitation/sale-invitation-request';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Page } from '../../model/util/page';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LicenseProduct } from '../../model/product/license-product';
import { PinkToast } from '../toastr/pink.toast';
import PageMethodeModelInfoReturn = DataTables.PageMethodeModelInfoReturn;

import { cloneDeep } from 'lodash-es';
import {ToastrService} from 'ngx-toastr';
import {QrCodeService} from '../../service/qr-code.service';
import {OrderDetail} from '../../model/order/order-detail';

@Component({
    selector: 'app-sale-invitations',
    templateUrl: './sale-invitations.component.html',
    styleUrls: ['./sale-invitations.component.css']
})
export class SaleInvitationsComponent implements AfterViewInit, OnInit, OnDestroy {

    @ViewChild("saleInvitationFormModal", {static: true}) saleInvitationFormModal: TemplateRef<any>;
    @ViewChild("saleInvitationOrderModal", {static: true}) saleInvitationOrderModal: TemplateRef<any>;

    @ViewChild(DataTableDirective, {static: false})
    private usersDatatableElement: DataTableDirective;
    invitationsTableTrigger: Subject<any> = new Subject();
    invitationsOpts: DataTables.Settings = {};
    dtInstance: DataTables.Api;
    saleInvitationsCallback: any;
    saleInvitationsPageInfo: PageMethodeModelInfoReturn;

    spinnerText: string;

    orderDetailInvitation: OrderDetail;
    saleInvitation: SaleInvitationRequest;
    saleInvitations: SaleInvitationInfo[];

    licenseProducts: LicenseProduct[];

    constructor(private router: Router,
                public authService: AuthService,
                public spinner: NgxSpinnerService,
                private toastr: ToastrService,
                public modalService: NgbModal,
                public saleInvitationService: SaleInvitationsService,
                public qrCodeService: QrCodeService,
                public dataStorageService: DataStorageService) {

        if (!this.authService.isAuthenticated())
            this.router.navigate(['/login']);
    }

    ngOnInit() {

        this.saleInvitation = new SaleInvitationRequest();

        this.licenseProducts = this.dataStorageService.licenseProducts;

        let defaultLicenseProduct = this.licenseProducts.filter(lp =>
            lp.name.toLowerCase().includes('pentagon'))[0];

        this.saleInvitation.licenseProductId = defaultLicenseProduct.id;

        this.invitationsOpts = this.defaultOptions();
        this.invitationsOpts.drawCallback = (settings) => {};

        this.invitationsOpts.ajax = (dataTablesParameters: any, callback, settings) => {
            this.usersDatatableElement.dtInstance.then((dtInstance: DataTables.Api) => {

                this.dtInstance = dtInstance;

                let pageInfo = dtInstance.page.info();
                let search = dtInstance.search();

                if (search.length == 1)
                    return;

                this.saleInvitationsCallback = callback;
                this.saleInvitationsPageInfo = pageInfo;

                this.spinnerMessage('Cargando invitaciones');

                this.loadSaleInvitationsTable(pageInfo, callback, () => {});

            });
        };
        this.invitationsOpts.columns = [
            {data: 'user.email'},
            {data: 'id'}
        ];

    }

    loadSaleInvitationsTable(pageInfo, dtCallback, afterLoadCallback) {

        this.saleInvitationService.findInvitations(this.dtInstance.search(), pageInfo.page, pageInfo.length)
            .subscribe(ulp => {
                this.loadSaleInvitationsPage(ulp, dtCallback);
                afterLoadCallback();
            });
    }

    loadSaleInvitationsPage(saleInvitationsPage: Page<SaleInvitationInfo>, callback) {

        this.saleInvitations = saleInvitationsPage.content;

        if (this.saleInvitations.length > 0) {
            callback({
                recordsTotal: saleInvitationsPage.totalElements,
                recordsFiltered: saleInvitationsPage.totalElements,
                data: []
            });
        }

        this.saleInvitation.email = null;
        this.modalService.dismissAll();
        this.spinner.hide('saleInvitationsSpinner');
    }

    defaultOptions() {

        return {
            responsive: true,
            language: {
                lengthMenu: 'Mostrar _MENU_ registros',
                search: 'Buscar: ',
                info: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
                searchPlaceholder: 'Email',
                paginate: {
                    first: "Primero",
                    last: "Último",
                    next: "Siguiente",
                    previous: "Anterior"
                }
            },
            pageLength: 3,
            lengthMenu: [5, 10, 15],
            searchDelay: 350,
            processing: false, serverSide: true, info: false, lengthChange: false, searching: true,
            columnDefs: [
                { targets: '_all', orderable: false, searchable: false }
            ]
        };

    }

    openCreateSaleInvitation() {

        this.modalService.open(this.saleInvitationFormModal, {
            ariaLabelledBy: 'modal-basic-title',
            centered: true
        }).result.then(
            (result) => {},
            (reason) => {});

    }

    closeCreateSaleInvitation() {
        this.saleInvitation.email = null;
        this.modalService.dismissAll();
    }

    createSaleInvitation() {

        this.spinnerMessage('Enviando invitación');

        this.saleInvitationService.createInvitation(this.saleInvitation)
            .subscribe(message => {
                this.loadSaleInvitationsTable(
                    this.saleInvitationsPageInfo,
                    this.saleInvitationsCallback, () => {
                        this.toastMessage("Invitación enviada", 1500);
                    });
            });

    }

    forwardSaleInvitation(saleInvitation: SaleInvitationInfo) {

        this.spinnerMessage('Reenviando invitación');

        this.saleInvitationService.forwardInvitation(saleInvitation.id)
            .subscribe(message => {
                this.loadSaleInvitationsTable(
                    this.saleInvitationsPageInfo,
                    this.saleInvitationsCallback, () => {
                        this.toastMessage("Invitación reenviada", 1500);
                    }
                );
            });

    }

    cancelSaleInvitation(saleInvitation: SaleInvitationInfo) {

        this.spinnerMessage('Cancelando invitación');
        this.spinner.show('saleInvitationsSpinner');

        this.saleInvitationService.cancelInvitation(saleInvitation.id)
            .subscribe(message => {
                this.loadSaleInvitationsTable(
                    this.saleInvitationsPageInfo,
                    this.saleInvitationsCallback, () => {
                        this.toastMessage("Invitación cancelada", 1500)
                    }
                );
            });

    }

    showOrderInvitationDetail(saleInvitation: SaleInvitationInfo) {

        this.orderDetailInvitation = saleInvitation.productOrder.orderDetails[0];

        this.modalService.open(this.saleInvitationOrderModal, {
            ariaLabelledBy: 'modal-basic-title',
            centered: true
        }).result.then(
            (result) => {},
            (reason) => {});

    }

    sendQRCode(saleInvitation: SaleInvitationInfo) {

        this.spinnerMessage('Enviando QR');
        this.spinner.show('saleInvitationsSpinner');

        this.qrCodeService.sendQRCode(saleInvitation.email)
            .subscribe(message => {
                this.loadSaleInvitationsTable(
                    this.saleInvitationsPageInfo,
                    this.saleInvitationsCallback, () => {
                        this.toastMessage("QR enviado", 1500)
                    }
                );
            });

    }

    reload() {

        this.spinnerMessage('Actualizando información');

        this.loadSaleInvitationsTable(
            this.saleInvitationsPageInfo,
            this.saleInvitationsCallback, () => {}
        );

    }

    toastMessage(message: string, time: number) {

        const opt = cloneDeep(this.toastr.toastrConfig);
        opt.toastComponent = PinkToast;
        opt.toastClass = 'pinktoast';
        opt.positionClass = 'toast-top-right-custom';
        opt.timeOut = time;

        this.toastr.show(message, 'Correcto!', opt);
    }

    spinnerMessage(message: string) {
        this.spinnerText = message;
        this.spinner.show('saleInvitationsSpinner');
    }

    ngAfterViewInit(): void {
        this.invitationsTableTrigger.next();
    }

    ngOnDestroy(): void {
        this.invitationsTableTrigger.unsubscribe();
    }

}
