import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ChangeEmailAccountComponent } from './components/change-email-account/change-email-account.component';
import { AccountComponent } from './components/account/account.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutModule } from './layout';
import { ReactiveFormsModule } from '@angular/forms';
import { OAuthModule } from 'angular-oauth2-oidc';
import { TokenInterceptor } from './components/auth/service/interceptor/token-interceptor';
import { ErrorInterceptor } from './components/auth/service/interceptor/error-interceptor';
import { AuthModule } from './components/auth/auth.module';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { PaymentMethodComponent } from './components/payment-method/payment-method.component';
import { AddPaymentMethodFormComponent } from './components/payment-method/components/create/add-payment-method-form.component';
import { BillingInformationComponent } from './components/billing-information/billing-information.component';
import { DataStorageService } from './data/data-storage.service';
import { ChangeMembershipComponent } from './components/change-membership/change-membership.component';
import { LicenseProductItemComponent } from './components/common/components/license-product-item/license-product-item.component';
import { EditPaymentMethodFormComponent } from './components/payment-method/components/edit/edit-payment-method-form.component';
import { SaleInvitationsComponent } from './components/sale-invitations/sale-invitations.component';
import { MembershipResumeComponent } from './components/change-membership/components/membership-resume/membership-resume.component';
import { LicenseProductVisualizerComponent } from './components/license-product-visualizer/license-product-visualizer.component';
import { UserLicensesManagerComponent } from './components/user-licenses-manager/user-licenses-manager.component';
import { DataTablesModule } from 'angular-datatables';

import { NotyfToast } from './components/toastr/notyf.toast';
import { PinkToast } from './components/toastr/pink.toast';
import { ActivateUserComponent } from './components/non-authenticated/components/activate-user/activate-user.component';
import { CancelMembershipComponent } from './components/cancel-membership/cancel-membership.component';
import { RestorePasswordComponent } from './components/non-authenticated/components/restore-password/restore-password.component';
import {ForgotPasswordComponent} from './components/non-authenticated/components/forgot-password/forgot-password.component';

@NgModule({
    declarations: [
        AppComponent,
        ChangePasswordComponent,
        ChangeEmailAccountComponent,
        PaymentMethodComponent,
        NotyfToast,
        PinkToast,
        BillingInformationComponent,
        AddPaymentMethodFormComponent,
        ChangeMembershipComponent,
        LicenseProductItemComponent,
        EditPaymentMethodFormComponent,
        AccountComponent,
        LicenseProductVisualizerComponent,
        ActivateUserComponent,
        RestorePasswordComponent,
        ForgotPasswordComponent,
        SaleInvitationsComponent,
        CancelMembershipComponent,
        MembershipResumeComponent,
        UserLicensesManagerComponent,
        AccountComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        DataTablesModule,
        FormsModule,
        AppRoutingModule,
        NgxSpinnerModule,
        LayoutModule,
        NgbModule,
        BrowserAnimationsModule,
        CreditCardDirectivesModule,
        OAuthModule.forRoot(),
        ToastrModule.forRoot(),
        HttpClientModule,
        AuthModule
    ],
    exports: [
        LayoutModule
    ],
    providers: [
        { provide: DataStorageService, useClass: DataStorageService },
        { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    entryComponents: [NotyfToast, PinkToast],
    bootstrap: [AppComponent]
})
export class AppModule { }
